# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Circle Game.
#
#     Circle Game is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Circle Game is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Circle Game.  If not, see <https://www.gnu.org/licenses/>.

from random import randint

from Circle import Circle


class Circles:

    def __init__(self, num_circles, screen, XSCREEN, YSCREEN):
        """
        Create the circles with a randomly position and if one circle is along with other, change their position
        :param num_circles: Number of circles to create
        :param screen: Necessary to create the circles
        :param XSCREEN: Maximum size  of the X axis of the game window
        :param YSCREEN: Maximum size  of the Y axis of the game window
        """
        self.circles = []
        self.user_circle = 0
        self.user_color = 0
        self.user_dead = False
        self.list_user_color = [(255, 255, 255), (255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255),
                                (0, 255, 255)]
        for i in range(0, num_circles):
            self.circles.append(Circle(screen, randint(0, XSCREEN), randint(0, YSCREEN), XSCREEN, YSCREEN))

        self.circles[self.user_circle].change_color(self.list_user_color[self.user_color])
        crashed = self.search_crash()
        while crashed[0] != -1:
            self.circles[crashed[0]] = Circle(screen, randint(0, XSCREEN), randint(0, YSCREEN), XSCREEN, YSCREEN)
            crashed = self.search_crash()

    def draw(self):
        """
        Draw all the circles available
        """
        for i in range(0, len(self.circles)):
            self.circles[i].draw()

    def move_user(self, down, up, right, left):
        """
        Move the user circle
        :param down: Boolean value to move down the circle
        :param up: Boolean value to move up the circle
        :param right: Boolean value to move right the circle
        :param left: Boolean value to move left the circle
        """
        if self.user_circle < len(self.circles):
            self.circles[self.user_circle].move(down, up, right, left)

    def move_computer(self):
        """
        Move all circles except the user circle
        """
        for i in range(0, len(self.circles)):
            if i != self.user_circle:
                self.circles[i].aleatory_movement()

    def search_crash(self):
        """
        Search the circles crashed.
        The method to do this is comparing their distances with their sum of their radius on the x axis and the y axis
        :return: First two circles crashed
        """
        for i in range(0, len(self.circles)):
            if i + 1 < len(self.circles):
                for j in range(i + 1, len(self.circles)):
                    if abs(self.circles[i].posx - self.circles[j].posx) < self.circles[i].radius + self.circles[
                        j].radius \
                            and abs(self.circles[i].posy - self.circles[j].posy) < self.circles[i].radius + \
                            self.circles[j].radius:
                        return [i, j]

        return [-1, -1]

    def change_color(self):
        """
        Change the color of the user circle
        """
        if self.user_circle < len(self.circles):
            self.user_color += 1
            if self.user_color < len(self.list_user_color):
                self.circles[self.user_circle].change_color(self.list_user_color[self.user_color])
            else:
                self.user_color = 0
                self.circles[self.user_circle].change_color(self.list_user_color[self.user_color])

    def change_user(self, previous):
        """
        Change the user circle to the next or previous circle in the list
        :param previous: If true, change to the previous circle instead of the next
        """
        if self.user_circle < len(self.circles):
            self.circles[self.user_circle].change_color((randint(0, 255), randint(0, 255), randint(0, 255)))

        if self.user_circle > len(self.circles) and previous:
            self.user_circle = len(self.circles) - 1
        else:
            if previous:
                self.user_circle -= 1
                if self.user_circle < 0:
                    self.user_circle = len(self.circles) - 1
            else:
                self.user_circle += 1
                if self.user_circle >= len(self.circles):
                    self.user_circle = 0

        self.circles[self.user_circle].change_color(self.list_user_color[self.user_color])
        self.user_dead = False

    def eat(self):
        """
        Call a search_crash function for determinate what circle is eaten and eliminated? and
            increase the radius of the winner circle.
        If one circle is bigger than other, the bigger eat the other.
        In case of equality, the second circle eat the other but if the second circle is the user circle,
            the user circle always win
        Before remove the eaten circle call a check_user function
        """
        crashed = self.search_crash()
        if crashed[0] != -1:
            if self.circles[crashed[0]].radius < self.circles[crashed[1]].radius:
                self.circles[crashed[1]].increase_radius(self.circles[crashed[0]].radius)
                self.circles.remove(self.circles.__getitem__(crashed[0]))
                self.check_user(crashed[0])
            elif self.circles[crashed[0]].radius > self.circles[crashed[1]].radius:
                self.circles[crashed[0]].increase_radius(self.circles[crashed[1]].radius)
                self.circles.remove(self.circles.__getitem__(crashed[1]))
                self.check_user(crashed[1])
            elif crashed[0] != self.user_circle:
                self.circles[crashed[1]].increase_radius(self.circles[crashed[0]].radius)
                self.circles.remove(self.circles.__getitem__(crashed[0]))
                self.check_user(crashed[0])
            else:
                self.circles[crashed[0]].increase_radius(self.circles[crashed[1]].radius)
                self.circles.remove(self.circles.__getitem__(crashed[1]))
                self.check_user(crashed[1])

    def check_user(self, removed):
        """
        Check if the user circle change before remove other circle, in this case this function correct the variable
            user_circle to maintain the reference but if the removed circle was the user_cicle, the value change to a
            unreferenced circle.
        :param removed: the value of the removed circle.
        """
        if removed < self.user_circle and not self.user_dead:
            self.user_circle -= 1
        elif removed == self.user_circle:
            self.user_dead = True
            self.user_circle = len(self.circles)
