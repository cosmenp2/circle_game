# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Circle Game.
#
#     Circle Game is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Circle Game is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Circle Game.  If not, see <https://www.gnu.org/licenses/>.

from random import randint

import pygame


class Circle:

    def __init__(self, screen, posx, posy, XSCREEN, YSCREEN):
        """
        Create the circle in their position indicated and gives a color different to blank
        :param screen: Necessary to draw in the screen
        :param posx: Is the x position of the circle
        :param posy: Is the y position of the circle
        :param XSCREEN: Maximum size  of the X axis of the game window
        :param YSCREEN: Maximum size  of the Y axis of the game window
        """
        self.color = (randint(0, 255), randint(0, 255), randint(0, 255))
        self.XSCREEN = XSCREEN
        self.YSCREEN = YSCREEN
        while self.color == (255, 255, 255):
            self.color = (randint(0, 255), randint(0, 255), randint(0, 255))
        self.radius = 10
        self.posx = posx
        self.posy = posy
        self.screen = screen

    def draw(self):
        """
        Draw the circle with their attributes
        """
        pygame.draw.circle(self.screen, self.color, (self.posx, self.posy), self.radius)

    def move(self, down, up, right, left):
        """
        Move the circle depending on values boolean and call a check_screen function
        :param down: If this value is true, move the circle down
        :param up: If this value is true, move the circle up
        :param right: If this value is true, move the circle right
        :param left: If this value is true, move the circle left
        """
        if down:
            if self.posy < self.YSCREEN - self.radius:
                self.posy += 1
        if up:
            if self.posy > 0 + self.radius:
                self.posy -= 1
        if right:
            if self.posx < self.XSCREEN - self.radius:
                self.posx += 1
        if left:
            if self.posx > 0 + self.radius:
                self.posx -= 1

        self.check_screen()

    def aleatory_movement(self):
        """
        Move the circle randomly and call a check_screen function
        """
        for i in range(0, randint(1, 3)):
            self.posx += randint(-3, 3)
        for i in range(0, randint(1, 3)):
            self.posy += randint(-3, 3)

        self.check_screen()

    def check_screen(self):
        """
        checks if the circle is in the screen. If not, moves the circle to the appropriate border
        """
        if self.posy > self.YSCREEN - self.radius:
            self.posy = self.YSCREEN - self.radius
        if self.posy < 0 + self.radius:
            self.posy = 0 + self.radius
        if self.posx > self.XSCREEN - self.radius:
            self.posx = self.XSCREEN - self.radius
        if self.posx < 0 + self.radius:
            self.posx = 0 + self.radius

    def increase_radius(self, value):
        """
        Increase the radius of the circle
        :param value: value to increase radius
        """
        self.radius += value

    def change_color(self, color):
        """
        Change to a specific color of the circle
        :param color: Value of the specific color
        """
        self.color = color
