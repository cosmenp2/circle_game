#!/bin/python
# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Circle Game.
#
#     Circle Game is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Circle Game is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Circle Game.  If not, see <https://www.gnu.org/licenses/>.


import pygame

from Circles import Circles

XSCREEN = 500
YSCREEN = 500
pygame.init()
screen = pygame.display.set_mode((XSCREEN, YSCREEN))
clock = pygame.time.Clock()

done = False
move = [False, False, False, False]
circles = Circles(20, screen, XSCREEN, YSCREEN)


def manage_events(circles, move):
    """
    Manage the events of the keyboard and exit
    :param circles: Necessary for move the circle
    :param move: A Boolean vector that represent the movement of the cursor (down, up, right, left respectively)
    :return: True if user want exist or False if not
    """
    finish = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finish = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_n:
                circles.change_user(False)
            if event.key == pygame.K_b:
                circles.change_user(True)
            if event.key == pygame.K_SPACE:
                circles.change_color()
            if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                move[0] = True
            if event.key == pygame.K_UP or event.key == pygame.K_w:
                move[1] = True
            if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                move[2] = True
            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                move[3] = True
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                move[0] = False
            if event.key == pygame.K_UP or event.key == pygame.K_w:
                move[1] = False
            if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                move[2] = False
            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                move[3] = False

    circles.move_user(move[0], move[1], move[2], move[3])
    return finish


def clean_screen():
    """
    Clean the screen with black color
    """
    screen.fill((0, 0, 0))


def refresh_screen():
    """
    Velocity  of refresh
    """
    clock.tick(60)
    pygame.display.flip()


"""
Start the program
"""
while not done:
    clean_screen()
    done = manage_events(circles, move)
    circles.move_computer()
    circles.eat()
    circles.draw()

    refresh_screen()
